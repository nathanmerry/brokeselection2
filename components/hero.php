<section class="hero">
  <div class="container">
    <div class="hero__wrap row align-items-center">
      <div class="hero__col col-lg-5">
        <h1 class="hero__header">Buy Shares Online Today</h1>
        <ul class="hero__list">
          <li class="hero__list-item">
            <img src="./assets/media/img/Icon.png" alt="" class="hero__list-icon">
            <div class="hero__list-text">Nulla varius neque ac sodales pretium.</div>
          </li>
          <li class="hero__list-item">
            <img src="./assets/media/img/Icon.png" alt="" class="hero__list-icon">
            <div class="hero__list-text">Nulla varius neque ac sodales pretium.</div>
          </li>
          <li class="hero__list-item">
            <img src="./assets/media/img/Icon.png" alt="" class="hero__list-icon">
            <div class="hero__list-text">Nulla varius neque ac sodales pretium.</div>
          </li>
          <li class="hero__list-item">
            <img src="./assets/media/img/Icon.png" alt="" class="hero__list-icon">
            <div class="hero__list-text">Nulla varius neque ac sodales pretium.</div>
          </li>
        </ul>
        <a href="#" class="hero__cta">BUY/SELL NOW</a>
      </div>
      <div class="hero__col col-lg-7">
        <img src="./assets/media/img/graph.png" alt="" class="hero__graphph">
      </div>
    </div>
  </div>
</section>