<section class="recommend">
  <div class="container">
    <h2 class="recommend__header">How Sharethemarkets Guarantees You  the Best Broker Recommendation</h2>
    <div class="row recommend__wrap">
      <div class="col-md-4 col-lg-2">
        <img src="./assets/media/img/recommend/1.png" alt="" class="recommend__icon">
        <div class="recommend__item">Commissions & Fees</div>
      </div>
      <div class="col-md-4 col-lg-2">
        <img src="./assets/media/img/recommend/2.png" alt="" class="recommend__icon">
        <div class="recommend__item">Commissions & Fees</div>
      </div>
      <div class="col-md-4 col-lg-2">
        <img src="./assets/media/img/recommend/3.png" alt="" class="recommend__icon">
        <div class="recommend__item">Commissions & Fees</div>
      </div>
      <div class="col-md-4 col-lg-2">
        <img src="./assets/media/img/recommend/4.png" alt="" class="recommend__icon">
        <div class="recommend__item">Commissions & Fees</div>
      </div>
      <div class="col-md-4 col-lg-2">
        <img src="./assets/media/img/recommend/5.png" alt="" class="recommend__icon">
        <div class="recommend__item">Commissions & Fees</div>
      </div>
      <div class="col-md-4 col-lg-2">
        <img src="./assets/media/img/recommend/6.png" alt="" class="recommend__icon">
        <div class="recommend__item">Commissions & Fees</div>
      </div>
    </div>
  </div>
</section>