<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,600,700,800&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./dist/global.css">
  <title>Broker Selection</title>
</head>

<body>
  <?php include './components/banner.php' ?>
  <?php include './components/hero.php' ?>
  <?php include './components/recommend.php' ?>
  <script src="./dist/global.js"></script>
</body>

</html>